#!/bin/bash
#
# cleanup refs.bib

refs_orig="refs_orig.bib"
refs="refs.bib"

# leave doi
  #| grep --invert-match '^doi' \
cat $refs_orig \
  | grep --invert-match '^doi' \
  | grep --invert-match 'url' \
  | grep --invert-match '^keywords' \
  | grep --invert-match '^issn' \
  | grep --invert-match '^isbn' \
  | grep --invert-match '^month' \
  | grep --invert-match '^day' \
  | grep --invert-match '^abstract' \
  | sed -e 's/TechReport/Manual/g' \
  > $refs

