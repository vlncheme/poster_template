#!/bin/bash
#
# move full lib here and clean

BIB_DIR=/home/tfawcett/research/bib

REF_FILE=$(ls $BIB_DIR | grep walton | grep 2019 | grep methods)
REF_FILE="${BIB_DIR}/${REF_FILE}"

cp "$REF_FILE" ./refs_orig.bib
./clean_refs.sh
